<?php
include_once("Persona.php");

/** Obtenemos el id de la persona a editar */
$idPersona = $_GET['id'];

$db = new mysqli("localhost", "root", "", "bdejemplo");

if($db->connect_errno) {
    print "Error en la conexión " . $db->connect_errno;
    exit();
}

$stmt = "select * from personas where id = $idPersona";
$result = $db->query($stmt);

$personaActual = new Persona();

if(mysqli_num_rows($result) > 0)
{
    $array_persona = $result->fetch_assoc();
    $personaActual = new Persona($array_persona['id'], $array_persona['nombre'], 
                                $array_persona['edad'], $array_persona['sexo']);
}

?>

<form method="POST" action="actualizarPersona.php">
    <p>Id: <input type="number" name="id" value="<?php echo $personaActual->id; ?>" <?php echo "readonly"; ?> /></p>

    <p>Nombre: <input type="text" name="nombre" value="<?php echo $personaActual->nombre; ?>" /></p>
    <p>Edad: <input type="number" name="edad" value="<?php echo $personaActual->edad; ?>" /></p>
    <p>Sexo: <select name="sexo">
            <option value="Masculino" <?php if ($personaActual->sexo == "Masculino") echo "selected"; ?>>
                Masculino
            </option>
            <option value="Femenino" <?php if ($personaActual->sexo == "Femenino") echo "selected"; ?>>
                Femenino
            </option>
        </select></p>

    <p><input type="submit" name="actualizarPersona" value="Guardar" /></p>
</form>

<a href="listarPersonas.php">Regresar</a>