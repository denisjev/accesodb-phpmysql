<?php

include_once("Persona.php");

$db = new mysqli("localhost", "root", "", "bdejemplo");

if($db->connect_errno) {
    print "Error en la conexión " . $db->connect_errno;
    exit();
}

$stmt = "select * from personas";
$result = $db->query($stmt);

$listaPersonas = array();

for($i=0; $i < mysqli_num_rows($result); $i++)
{
    $array_persona = $result->fetch_assoc();
    $listaPersonas[$i] = new Persona($array_persona['id'], $array_persona['nombre'], 
                                $array_persona['edad'], $array_persona['sexo']);
}

?>


<table>
    <tr><th>Id</th><th>Nombre</th><th>Edad</th><th>Sexo</th><th>Acciones</th></tr>
<?php

foreach($listaPersonas as $persona)
{
    echo "<tr><td>" . $persona->id .  "</td><td>" . 
                    $persona->nombre .  "</td><td>" . 
                    $persona->edad .  "</td><td>" . 
                    $persona->sexo . "</td><td>" .
                    "<a href='editarPersona.php?id=" . $persona->id . "'>Editar</a> " . 
                    "<a href='borrarPersona.php?id=" . $persona->id . "'>Borrar</a> " . 
                    "</td></tr>";
}

?>
</table>

<br />
<a href="index.php">Regresar</a>