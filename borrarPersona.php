<?php
include_once("Persona.php");

/** Obtenemos el id de la persona a editar */
$idPersona = $_GET['id'];

$db = new mysqli("localhost", "root", "", "bdejemplo");

if($db->connect_errno) {
    print "Error en la conexión " . $db->connect_errno;
    exit();
}

$stmt = "select * from personas where id = $idPersona";

$result = $db->query($stmt);

$personaActual = new Persona();

if(mysqli_num_rows($result) > 0)
{
    $array_persona = $result->fetch_assoc();
    $personaActual = new Persona($array_persona['id'], $array_persona['nombre'], 
                                $array_persona['edad'], $array_persona['sexo']);
}


?>

<form method="POST" action="eliminarPersona.php">
    <input type="hidden" name="id" value="<?php echo $personaActual->id; ?>" />
    <p>Nombre: <?php echo $personaActual->nombre; ?></p>
    <p>Edad: <?php echo $personaActual->edad; ?> </p>
    <p>Sexo: <?php echo $personaActual->sexo ?> </p>
    <br>
    <p>¿Seguro que desea eliminar el registro?</p>
    <p><input type="submit" name="eliminarPersona" value="Eliminar" /></p>
</form>

<a href="listarPersonas.php">Regresar</a>